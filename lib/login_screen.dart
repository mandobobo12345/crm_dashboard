import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.sizeOf(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 0),
        child: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
            image: AssetImage("logo.png"),
            fit: BoxFit.contain,
          )),
          padding: EdgeInsets.symmetric(
              horizontal: media.width * .32, vertical: media.height * .08),
          width: media.width,
          height: media.height,
          child: Container(
            width: media.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: const Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            padding: EdgeInsets.symmetric(vertical: media.height * .05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "logo.png",
                      width: media.width * .1,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      "StepsCrm",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: media.width * .017,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "Welcome! Let's get Started !",
                  style: TextStyle(
                    color: Colors.grey.shade500,
                    fontSize: media.width * .012,
                  ),
                ),
                SizedBox(
                  height: media.height * .08,
                ),
                textFileds(media),
                SizedBox(
                  height: media.height * .08,
                ),
                const Spacer(),
                RichText(
                  text: TextSpan(
                    text: "New User ? ",
                    style: TextStyle(color: Colors.grey.shade500),
                    children: <TextSpan>[
                      TextSpan(
                        text: "Sign Up",
                        recognizer: TapGestureRecognizer()..onTap = () {},
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  themeColor({required Widget child}) => Theme(
        data: Theme.of(context).copyWith(
          unselectedWidgetColor: Colors.grey,
        ),
        child: child,
      );
  Padding textFileds(Size media) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: media.width * .06),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 15),
            child: Card(
              elevation: 5.0,
              child: TextFormField(
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: "User Name",
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade200),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade200),
                  ),
                ),
                controller: TextEditingController(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Card(
              elevation: 5.0,
              child: TextFormField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Password",
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey.shade200),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey.shade200))),
                controller: TextEditingController(),
              ),
            ),
          ),
          rowForgotPassword(media),
          button(media)
        ],
      ),
    );
  }

  Padding rowForgotPassword(Size media) {
    return Padding(
      padding: EdgeInsets.only(top: media.height * .05),
      child: Row(
        children: [
          themeColor(
            child: Checkbox(
              activeColor: Colors.transparent,
              checkColor: Colors.indigo,
              value: isActive,
              onChanged: (value) {
                isActive = value!;
                setState(() {});
              },
            ),
          ),
          const Text("Keep me signed in"),
          const Spacer(),
          TextButton(
            onPressed: () {},
            child: const Text(
              "Forgot password",
              style: TextStyle(
                color: Colors.black,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding button(Size media) {
    return Padding(
      padding: EdgeInsets.only(top: media.height * .05),
      child: SizedBox(
        width: media.width,
        height: media.height * .07,
        child: ElevatedButton(
          onPressed: () {},
          style: ButtonStyle(
            shape: MaterialStatePropertyAll(
              RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(8),
              ),
            ),
            backgroundColor: const MaterialStatePropertyAll(
              Color(0xff4066f0),
            ),
          ),
          child: Text(
            "Login",
            style: TextStyle(color: Colors.white, fontSize: media.width * .012),
          ),
        ),
      ),
    );
  }
}
