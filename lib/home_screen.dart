import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'models.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> listOfselectRecurrence = ["Does not recur", "Custom"];
  String valueOfSelectRecurrence = " ";

  List<String> lisOfDataTime = ["Day", "Week", "Month"];
  String valueOfDataTime = " ";

  bool onDay = true;
  List<String> lisOfCount = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
    "24",
    "25",
    "26",
    "27",
    "28",
    "29",
    "30",
    "31",
    "32",
    "33",
    "34",
    "35",
    "36",
    "37",
    "38",
    "39",
    "40"
  ];

  String valueOfCount = " ";

  bool onThe = false;
  List<String> list = [
    "First",
    "Second",
    "Third",
    "Fourth",
    "last",
  ];

  String valuee = " ";

  bool on = true;
  bool after = false;

  DateTime? _selectedDate;

  TextEditingController controller = TextEditingController();
  TextEditingController occurrencesController = TextEditingController();

  List<String> weekDays = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];

  List<WeekModel> weekList = [
    WeekModel(day: 'Sunday', selected: false),
    WeekModel(day: 'Monday', selected: false),
    WeekModel(day: 'Tuesday', selected: false),
    WeekModel(day: 'Wednesday', selected: false),
    WeekModel(day: 'Thursday', selected: false),
    WeekModel(day: 'Friday', selected: false),
    WeekModel(day: 'Saturday', selected: false),
  ];

  List<String> selectedDays = [];

  String valueOfWeeks = " ";

  @override
  void initState() {
    super.initState();
    valueOfSelectRecurrence = listOfselectRecurrence[0];

    valueOfDataTime = lisOfDataTime[0];

    valueOfCount = lisOfCount[0];

    valuee = list[0];

    valueOfWeeks = weekDays[0];
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.sizeOf(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Center(
          child: Column(
            children: [
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: selectRecurrenceWidget(),
                    ),
                    Expanded(
                      child: repeatEveryWidget(),
                    ),
                    Expanded(
                      child: selectDataTime(),
                    ),
                  ]),
              if (valueOfDataTime == "Month") ...[
                monthWidget(media),
                onTheWidget(media),
              ] else if (valueOfDataTime == "Week") ...[
                weekWidget(media)
              ] else ...[
                const SizedBox()
              ],
              endsWidget(context, media)
            ],
          ),
        ),
      ),
    );
  }

  Padding weekWidget(Size media) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Container(
        width: media.width,
        padding: EdgeInsets.only(left: media.width * .335),
        child: Row(
          children: [
            const Text("Repeat on"),
            Padding(
              padding: EdgeInsets.only(left: media.width * .08),
              child: SizedBox(
                height: 50,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: weekList.length,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () {
                      if (selectedDays.contains(weekList[index].day)) {
                        selectedDays.remove(weekList[index].day);
                        weekList[index].selected = false;
                      } else {
                        weekList[index].selected = true;
                        selectedDays.add(weekList[index].day);
                      }
                      if (kDebugMode) {
                        print("Day Selected : $selectedDays");
                      }
                      setState(() {});
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: weekList[index].selected == true
                              ? Colors.grey.shade300
                              : Colors.transparent,
                          border: Border.all(color: Colors.black)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(weekList[index].day.toString()),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  } // ============ # weekWidget end HERE ============

  Container monthWidget(Size media) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: media.width * .33),
      child: rowSelectedDataTime(
          text: "on day",
          dropList: lisOfCount,
          valueOfDropList: valueOfCount,
          valueCheckBox: onDay,
          onChangedCheckBox: (bool? value) {
            setState(() {
              onDay = value!;
              onThe = false;
            });
          },
          onChangedDropList: (value) {
            valueOfCount = value!;
            setState(() {});
          }),
    );
  } // ============ # monthWidget end HERE ============

  Row endsWidget(BuildContext context, Size media) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          flex: 2,
          child: Container(
              padding: const EdgeInsets.only(right: 50),
              alignment: Alignment.bottomRight,
              child: const Text("Ends")),
        ),
        Expanded(
            flex: 2,
            child: Container(
              padding: const EdgeInsets.only(left: 150),
              alignment: Alignment.topRight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  themeColor(
                    child: Checkbox(
                      activeColor: Colors.transparent,
                      shape: const CircleBorder(),
                      checkColor: Colors.indigo,
                      value: on,
                      onChanged: (value) {
                        on = value!;
                        after = false;
                        setState(() {});
                      },
                    ),
                  ),
                  const Text("On"),
                  const SizedBox(width: 50),
                  themeColor(
                    child: Checkbox(
                      activeColor: Colors.transparent,
                      shape: const CircleBorder(),
                      checkColor: Colors.indigo,
                      value: after,
                      onChanged: (value) {
                        after = value!;
                        on = false;
                        setState(() {});
                      },
                    ),
                  ),
                  const Text("After"),
                ],
              ),
            )),
        if (on == true) ...[
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: "Select date",
                      suffixIcon: Icon(
                        Icons.date_range,
                      ),
                    ),
                    onTap: () => selectDate(context),
                    controller: controller,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Expanded(child: Text("Occurrences"))
              ],
            ),
          )
        ] else ...[
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: occurrencesController,
                  ),
                ),
                const Text("Occurrences")
              ],
            ),
          )
        ]
      ],
    );
  } // ============ # endsWidget end HERE ============

  Container onTheWidget(Size media) {
    return Container(
      padding: EdgeInsets.only(left: media.width * .33),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            children: [
              themeColor(
                child: Checkbox(
                  activeColor: Colors.transparent,
                  shape: const CircleBorder(),
                  checkColor: Colors.indigo,
                  value: onThe,
                  onChanged: (bool? value) {
                    setState(() {
                      onThe = value!;
                      onDay = false;
                    });
                  },
                ),
              ),
              const Text("on the"),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 100),
            child: SizedBox(
              width: 320,
              child: DropdownButton<String>(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                value: valuee,
                isExpanded: true,
                dropdownColor: Colors.white,
                iconDisabledColor: Colors.black,
                onChanged: (value) {
                  valuee = value!;
                  setState(() {});
                },
                items: list.map((String valueItem) {
                  return DropdownMenuItem<String>(
                    value: valueItem,
                    child: Text(
                      valueItem,
                      style: const TextStyle(color: Colors.black),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          Expanded(
              child: DropdownButton<String>(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            value: valueOfWeeks,
            isExpanded: true,
            dropdownColor: Colors.white,
            iconDisabledColor: Colors.black,
            onChanged: (value) {
              valueOfWeeks = value!;
              setState(() {});
            },
            items: weekDays.map((String valueItem) {
              return DropdownMenuItem<String>(
                value: valueItem,
                child: Text(
                  valueItem,
                  style: const TextStyle(color: Colors.black),
                ),
              );
            }).toList(),
          )),
        ],
      ),
    );
  } // ============ # ontTheWidget end HERE ============

  Container repeatEveryWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Expanded(child: Text("Repeat every")),
          Expanded(
              flex: 2,
              child: SizedBox(
                height: 40,
                child: TextFormField(
                  controller: TextEditingController(),
                ),
              )),
        ],
      ),
    );
  } // ============ # repeatEveryWidget end HERE ============

  Widget selectRecurrenceWidget() {
    return DropdownButton<String>(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      value: valueOfSelectRecurrence,
      isExpanded: true,
      dropdownColor: Colors.white,
      iconDisabledColor: Colors.black,
      onChanged: (value) {
        valueOfSelectRecurrence = value!;
        setState(() {});
      },
      items: listOfselectRecurrence.map((String valueItem) {
        return DropdownMenuItem<String>(
          value: valueItem,
          child: Text(
            valueItem,
            style: const TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
    );
  } // ============ # selectRecurrenceWidget end HERE ============

  Widget selectDataTime() {
    return DropdownButton<String>(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      value: valueOfDataTime,
      isExpanded: true,
      dropdownColor: Colors.white,
      iconDisabledColor: Colors.black,
      onChanged: (value) {
        valueOfDataTime = value!;
        setState(() {});
      },
      items: lisOfDataTime.map((String valueItem) {
        return DropdownMenuItem<String>(
          value: valueItem,
          child: Text(
            valueItem,
            style: const TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
    );
  } // ============ # selectDataTime end HERE ============

  themeColor({required Widget child}) => Theme(
        data: Theme.of(context).copyWith(
          unselectedWidgetColor: Colors.grey,
        ),
        child: child,
      ); // ============ # themeColor end HERE ============

  Padding rowSelectedDataTime({
    required List<String> dropList,
    required String valueOfDropList,
    required bool valueCheckBox,
    required void Function(bool?)? onChangedCheckBox,
    required void Function(String?)? onChangedDropList,
    required String text,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Expanded(
            child: Row(
              children: [
                themeColor(
                  child: Checkbox(
                      activeColor: Colors.transparent,
                      shape: const CircleBorder(),
                      checkColor: Colors.indigo,
                      value: valueCheckBox,
                      onChanged: onChangedCheckBox),
                ),
                Text(text)
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: DropdownButton<String>(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              value: valueOfDropList,
              isExpanded: true,
              dropdownColor: Colors.white,
              iconDisabledColor: Colors.black,
              onChanged: onChangedDropList,
              items: dropList.map((String valueItem) {
                return DropdownMenuItem<String>(
                  value: valueItem,
                  child: Text(
                    valueItem,
                    style: const TextStyle(color: Colors.black),
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  } // ============ # rowSelectedDataTime end HERE ============

  Future<void> selectDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );

    if (picked != null && picked != _selectedDate) {
      setState(() {
        controller.text = picked.toString().replaceAll("00:00:00.000", "");
        _selectedDate = picked;
      });
    }
  } // ============ # selectDate end HERE ============
}
