class WeekModel {
  final String day;
  bool? selected;

  WeekModel({required this.day, required this.selected});
}
